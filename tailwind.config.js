/* eslint-disable */

const colors = require("tailwindcss/colors");
const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  darkMode: false, // or 'media' or 'class'
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    fontFamily: {
      "airbnb-cereal-app-book": ['"Airbnb Cereal App Book"'],
      "airbnb-cereal-app-bold": ['"Airbnb Cereal App Bold"'],
      "airbnb-cereal-app-light": ['"Airbnb Cereal App Light"'],
      "airbnb-cereal-app-black": ['"Airbnb Cereal App Black"'],
      "airbnb-cereal-app-medium": ['"Airbnb Cereal App Medium"'],
      "airbnb-cereal-app-extra-bold": ['"Airbnb Cereal App Extra Bold"'],
    },
    colors: {
      ...colors,
      footer: {
          default: '#f7f7f7',
          color: '#777777'
      },
      blue: {
        default: "#0677CE",
        second: "#6BADDC",
      },
      black: {
        default: "#222222",
        second: "#131313",
      },
      below: {
          default: '#f7f7f7'
      },
      video: {
          default: '#34383C'
      },
      light: '#868993',
      course: {
          default: '#fcfcfc',
          second: '#868993',
          blue: '#6BADDC'
      },
      quiz: {
        default: '#D2ECFF',
        green: '#00BF64',
        red: '#FF0021',
      },
      exercise: {
          gray: '#ECECEC'
      },
      settings: {
          default: "#5F5F5F"
      },
      scrollbar: {
          gray: '#CACACA'
      }
    },
    container: {
      center: true,
      padding: {
        DEFAULT: '2rem',
        sm: '2rem',
        lg: '3rem',
        xl: '4rem',
        '2xl': '5rem',
      },
    },
    maxHeight: {
      navbar: "100px",
    },
    minHeight: {
      navbar: "100px",
    },
    extend: {
        backgroundImage: theme => ({
            'banner': "url('https://res.cloudinary.com/mukn/image/upload/v1630507431/images/banner_krw5tm.png')",
            'elipse': "url('https://res.cloudinary.com/mukn/image/upload/v1630507410/images/double-elipse_etz1wn.png')",
        }),
        height: {
            'editor-bar': '50px',
            'card-img': '300px',
            'double-elipse-md': '1024px',
            'double-elipse-lg': '800px',
            'double-elipse-xl': '800px',
        },
        width: {
            'card-img': '280px',
            'double-elipse-md': '460px',
            'double-elipse-lg': '800px',
            'double-elipse-xl': '800px',
        },
        translate: {
            'account-dropdown': '7%'
        }
    },
  },
  variants: {
    extend: {
      opacity: ['disabled'],
    },
  },
  plugins: [
      require('@tailwindcss/forms'),
      require('tailwind-scrollbar'),
    ],
};
