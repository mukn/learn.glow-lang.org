# Learn Glow official repository

![Learn Glow](/src/assets/images/LOGO.png "Learn Glow")

This repository contains the official source code of the learning website, Learn Glow. [https://learn-glow.org](https://learn-glow.org).

***

## 1. Installation

<ins>**1. Downloading the repository:**</ins>

Start by cloning the repository with `git clone --branch cli https://gitlab.com/moussa-ball/learn-glow.git`.

<ins>**2. Switch to the repo folder:**</ins>

Switch to the repo folder.

```
$ cd learn-glow
```


<ins>**3. Install dependencies:**</ins>

```
$ npm install
```

<ins>**4. Compiles and hot-reloads for development:**</ins>

```
$ npm run serve
```

<ins>**5. Compiles and minifies for production:**</ins>

```
npm run build
```

<ins>**6. Lints and fixes files:**</ins>

```
npm run lint
```

## 2. Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
