
import globalComponents from "./globalComponents";
import globalDirectives from "./globalDirectives";
import globalMethods from "./globalMethods";

export default {
    install(Vue) {
        Vue.use(globalComponents);
        Vue.use(globalDirectives);
        Vue.use(globalMethods);
    }
};