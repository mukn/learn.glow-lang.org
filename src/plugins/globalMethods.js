const Notify = function(message, type = 'success') {
  // eslint-disable-next-line no-undef
  new Noty({
    text: '<strong>' + message + '</strong>',
    layout: 'topRight',
    type: type,
    theme: 'metroui',
    timeout: 5000,
    progressBar: true
  }).show()
}

const GlobalMethods = {
  install(Vue) {
    Vue.prototype.showError = Notify
  }
}

export default GlobalMethods
