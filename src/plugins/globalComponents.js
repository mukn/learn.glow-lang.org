import Nl2br from 'vue-nl2br'
import VueCarousel from 'vue-carousel'
var VueScrollTo = require('vue-scrollto')
import * as components from '@/components'
import Transitions from 'vue2-transitions'

import VuePlyr from 'vue-plyr'
import 'vue-plyr/dist/vue-plyr.css'

export default {
  install(Vue) {
    // Import All vue components in components folder.
    Object.entries(components).forEach(([name, component]) => {
      Vue.component(name, component)
    })

    // Import components
    Vue.component('nl2br', Nl2br)
    Vue.use(VueCarousel)
    Vue.use(VueScrollTo)
    Vue.use(Transitions)

    Vue.use(VuePlyr, {
      plyr: {}
    })
  }
}
