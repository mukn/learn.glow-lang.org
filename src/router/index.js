import Vue from 'vue'
import firebase from 'firebase'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// Import Layouts
import Navbar from '../layouts/Navbar'
import Footer from '../layouts/Footer'
import Navbar2 from '../layouts/Navbar2'
import Footer2 from '../layouts/Footer2'

// Import Pages
import Home from '../views/Home'
import Quiz from '../views/Quiz'
import Course from '../views/Course'
import Courses from '../views/Courses'
import Modules from '../views/Modules'
import Exercise from '../views/Exercise'
import FaqShow from '../views/Faqs/Show'
import NotFound from '../views/NotFound'
import FaqIndex from '../views/Faqs/Index'
import PrivacyPolicy from '../views/PrivacyPolicy'
import TermsConditions from '../views/TermsConditions'

// Authentification
import Login from '../views/auth/Login'
import Verify from '../views/auth/Verify'
import Profile from '../views/auth/Profile'
import Register from '../views/auth/Register'
import Settings from '../views/auth/Settings'
import ForgotPassword from '../views/auth/ForgotPassword'

// All routes.
const routes = [
  {
    name: 'not-found',
    path: '*',
    components: {
      navbar: Navbar,
      default: NotFound,
      footer: Footer
    }
  },
  {
    name: 'home',
    path: '/',
    components: {
      navbar: Navbar,
      default: Home,
      footer: Footer
    }
  },
  {
    name: 'courses',
    path: '/courses',
    components: {
      navbar: Navbar,
      default: Courses,
      footer: Footer
    }
  },
  {
    name: 'modules',
    path: '/modules',
    components: {
      navbar: Navbar,
      default: Modules,
      footer: Footer
    }
  },
  {
    name: 'course',
    path: '/course',
    components: {
      navbar: Navbar,
      default: Course,
      footer: Footer
    }
  },
  {
    name: 'faqs.index',
    path: '/faqs',
    components: {
      navbar: Navbar,
      default: FaqIndex,
      footer: Footer
    }
  },
  {
    name: 'faqs.show',
    path: '/faqs/:tag/:slug',
    components: {
      navbar: Navbar,
      default: FaqShow,
      footer: Footer
    }
  },
  {
    name: 'privacy-policy',
    path: '/privacy-policy',
    components: {
      navbar: Navbar,
      default: PrivacyPolicy,
      footer: Footer
    }
  },
  {
    name: 'terms-conditions',
    path: '/terms-conditions',
    components: {
      navbar: Navbar,
      default: TermsConditions,
      footer: Footer
    }
  },
  {
    name: 'quiz',
    path: '/quiz',
    components: {
      navbar: Navbar2,
      default: Quiz,
      footer: Footer2
    }
  },
  {
    name: 'exercise',
    path: '/exercise/:slug',
    component: Exercise
  },
  {
    name: 'register',
    path: '/register',
    components: {
      navbar: Navbar,
      default: Register,
      footer: Footer
    },
    meta: { requiresAuth: false }
  },
  {
    name: 'login',
    path: '/login',
    components: {
      navbar: Navbar,
      default: Login,
      footer: Footer
    },
    meta: { requiresAuth: false }
  },
  {
    name: 'forgot',
    path: '/forgot-password',
    components: {
      navbar: Navbar,
      default: ForgotPassword,
      footer: Footer
    },
    meta: { requiresAuth: false }
  },
  {
    name: 'verify',
    path: '/verify-email',
    components: {
      navbar: Navbar,
      default: Verify,
      footer: Footer
    },
    meta: { requiresAuth: true }
  },
  {
    name: 'settings',
    path: '/settings',
    components: {
      navbar: Navbar,
      default: Settings,
      footer: Footer
    },
    meta: { requiresAuth: true }
  },
  {
    name: 'profile',
    path: '/my-profile',
    components: {
      navbar: Navbar,
      default: Profile,
      footer: Footer
    },
    meta: { requiresAuth: true }
  }
]

/**
 * Create and export routes.
 */
const router = new VueRouter({
  mode: 'history',
  linkActiveClass: '',
  linkExactActiveClass: 'font-airbnb-cereal-app-bold text-blue-default hover:opacity-100',
  routes
})

router.beforeEach((to, from, next) => {
  const authenticated = firebase.auth().currentUser
  switch (to.name) {
    case 'login':
      if (authenticated) next('/')
      else next()
      break

    case 'register':
      if (authenticated) next('/')
      else next()
      break

    case 'verify':
      if (authenticated && authenticated.emailVerified) next('/')
      else if (authenticated && !authenticated.emailVerified) next()
      else next('/login')
      break

    case 'settings':
      if (authenticated && authenticated.emailVerified) next()
      else if (authenticated && !authenticated.emailVerified) next({ name: 'verify' })
      else next('/login')
      break

    case 'profile':
      if (authenticated && authenticated.emailVerified) next()
      else if (authenticated && !authenticated.emailVerified) next({ name: 'verify' })
      else next('/login')
      break

    default:
      next()
      break
  }
})

export default router
