import '@/assets/css/index.css'
import '@/assets/fonts/fonts.css'

window.$ = require('jquery')
window.jQuery = require('jquery')

import 'noty/src/noty.scss'
import 'noty/src/themes/metroui.scss'
import Noty from 'noty'

import Cloudinary from 'cloudinary-vue'

import Vue from 'vue'
import App from './App.vue'

import store from './store'
import router from './router'
import './registerServiceWorker'
import Glow from './plugins/Glow'

import firebase from 'firebase'

const firebaseConfig = {
  apiKey: 'AIzaSyDKQCKi0N1-U3fqWCS062NBCmkqJqKxdsk',
  authDomain: 'glow-lang.firebaseapp.com',
  databaseURL: 'https://glow-lang-default-rtdb.firebaseio.com',
  projectId: 'glow-lang',
  storageBucket: 'glow-lang.appspot.com',
  messagingSenderId: '556524449434',
  appId: '1:556524449434:web:c807da326fd1abf751b923',
  measurementId: 'G-LMH0NTBS2W'
}

// Initialize Firebase
firebase.initializeApp(firebaseConfig)

// Notifications
Vue.prototype.Notify = function(message, type = 'error') {
  new Noty({
    text: '<strong>' + message + '</strong>',
    layout: 'topRight',
    type: type,
    theme: 'metroui',
    timeout: 5000,
    progressBar: true
  }).show()
}

Vue.config.productionTip = false
Vue.use(Glow)

Vue.use(Cloudinary, {
  configuration: {
    cloudName: 'mukn'
  }
})

// Firebase listener
let app
firebase.auth().onAuthStateChanged(user => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App),
      created() {
        this.$store.dispatch('Authentication/user', user)
      }
    }).$mount('#app')
  }
})
